﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation
{
    public class Table
    {
        public int TableNumber { get; set; }
        public int Chairs { get; set; }
        public bool IsOcuppied { get; set; }
        public int OcuppiedCount { get; set; }
        public int OccupiedTime { get; set; }

        public List<Table> CreateTables()
        {
            var Tables = RandomInputs.Tables();
            List<Table> LstTable = new List<Table>();
            for (int i = 0; i < Tables; i++)
            {
                LstTable.Add(new Table
                {
                    TableNumber = i+1,
                    Chairs = RandomInputs.Chairs(),
                    OcuppiedCount = 0,
                    OccupiedTime = 0,
                    IsOcuppied = false
                });
            }
            return LstTable;
        }
    }
}
