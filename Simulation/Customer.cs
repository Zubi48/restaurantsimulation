﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation
{
    public class Customer
    {
        public bool IsGroup { get; set; }
        public int CustomerCount { get; set; }
        public int ArriveTime { get; set; }
        //public int LeaveTime { get; set; }

        public Customer CreateCustomer()
        {
            Customer cust = new Customer();
            cust.CustomerCount = RandomInputs.Customers();
            cust.IsGroup = RandomInputs.Group();
            cust.ArriveTime = RandomInputs.ArriveTime();
            //cust.LeaveTime = RandomInputs.LeaveTime();

            return cust;
        }
    }

}
