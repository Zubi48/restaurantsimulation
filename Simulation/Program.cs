﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Simulation;

namespace Simulation
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer = new Customer();
            Table table = new Table();
            List<Customer> LstCustomer = new List<Customer>();
            List<Table> LstTable = new List<Table>();
            int Timecount = 0, TimeInterval = 0 ;

            LstTable = table.CreateTables();

            int TotalCustomer = 0;

            while (Timecount <= 720)
            {
                TimeInterval = RandomInputs.ArriveTime();
                Timecount = Timecount + TimeInterval;
                var cust = customer.CreateCustomer();
                for (int i = 0; i < LstTable.Count-1; i++)
                {
                    if (cust.IsGroup == true && cust.CustomerCount <= LstTable[i].Chairs && LstTable[i].IsOcuppied == false)
                    {
                        TotalCustomer += cust.CustomerCount;
                        LstTable[i].OcuppiedCount += 1;
                        LstTable[i].OccupiedTime = RandomInputs.LeaveTime();
                        LstTable[i].IsOcuppied = true;
                        break;
                    }
                    else if (cust.IsGroup == false)
                    {
                        for (int j = 0; j < cust.CustomerCount-1; j++)
                        {
                            if (LstTable[i].Chairs >= 1 && LstTable[i].IsOcuppied == false)
                            {
                                TotalCustomer += 1;
                                LstTable[i].OcuppiedCount += 1;
                                LstTable[i].OccupiedTime = RandomInputs.LeaveTime(); ;
                                LstTable[i].IsOcuppied = true;
                                break;
                            }
                        }
                    }
                    else if (LstTable[i].OccupiedTime <= 0)
                    {
                        LstTable[i].IsOcuppied = false;
                    }
                    else if (LstTable[i].IsOcuppied)
                    {
                        LstTable[i].OccupiedTime -= TimeInterval;
                    }
                    
                }

            }
            var MostUsed = (from x in LstTable
                            select x).OrderByDescending(x => x.OcuppiedCount).Take(1);
            var LessUsed = (from x in LstTable
                            select x).OrderBy(x => x.OcuppiedCount).Take(1);


            Console.WriteLine("Restaurant Simulation");
            Console.WriteLine("Number of tables = " + LstTable.Count);
            Console.WriteLine("Total Customers = " + TotalCustomer);


            foreach (var item in MostUsed)
            {
                Console.WriteLine("The Most Used Table is the " +item.TableNumber+ " Occupied " +item.OcuppiedCount +" times");
            }
            foreach (var item in LessUsed)
            {
                Console.WriteLine("The Most Used Table is the " + item.TableNumber + " Occupied " + item.OcuppiedCount + " times");
            }
        }
    }
}
