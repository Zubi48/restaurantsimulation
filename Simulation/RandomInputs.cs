﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation
{
    class RandomInputs
    {
        public static Random rnd = new Random();

        public static int Tables()
        {
            return rnd.Next(5, 20);
        }

        public static int Chairs()
        {
            return rnd.Next(2, 10);
        }

        public static int Customers()
        {
            return rnd.Next(1, 10);
        }

        public static int ArriveTime()
        {
            return rnd.Next(10, 60);
        }

        public static int LeaveTime()
        {
            return rnd.Next(30, 90);
        }
        public static bool Group()
        {
            return rnd.Next(0, 2) == 1 ? true : false;
        }
    }
}
